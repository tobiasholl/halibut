#include "halibut.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
#define __USE_GNU
#endif

#include <cpuid.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/user.h>
#include <threads.h>
#include <ucontext.h>
#include <unistd.h>

#ifndef HALIBUT_NO_PTHREADS
#include <pthread.h>
#endif

#ifndef __x86_64__
    #error halibut: Requires compiling for x86-64
#endif
#ifndef __XSAVE__
    #error halibut: Requires support for XSAVE (compile with -mxsave)
#endif



/* ***************************** MISCELLANEOUS ***************************** */

/* Forward declarations */
static void pp_disable_preemption();
static void pp_enable_preemption();

/* Constants */
#define PP_PREEMPTION_USECS ((suseconds_t) 25000)
#define PP_INVALID_HANDLE ((size_t) -1)
#define PP_DIAGNOSTICS_ENV "HALIBUT_DIAGNOSTICS"

/* PP_ABORT: A wrapper for pp_abort that automatically handles code location data. */
#define PP_ABORT(...) pp_abort(__FILE__, __LINE__, __VA_ARGS__)

/* pp_abort: Abort execution with the specified error message. */
static void pp_abort(const char *file, int line, const char *error, ...)
{
    pp_disable_preemption();

    /* Initialize variadic argument handling. */
    va_list varargs;
    va_start(varargs, error);

    /* Write error message. */
    fprintf(stderr, "halibut (%s:%d): Fatal error: ", file, line);
    vfprintf(stderr, error, varargs);
    fprintf(stderr, "\n");

    /* Clean up and abort. */
    va_end(varargs);
    abort();
}

/* pp_xsave_area_size: Queries CPUID for the size of the XSAVE area. */
static size_t pp_xsave_area_size()
{
    thread_local static size_t xsave_area_size = 0;
    if (!xsave_area_size)
    {
        unsigned int cpuid_max = __get_cpuid_max(0, NULL);
        if (cpuid_max <= 0)
            PP_ABORT("CPUID not supported");

        /* Verify that necessary features are supported. */
        unsigned int eax = 0, ebx = 0, ecx = 0, edx = 0;
        __get_cpuid(1, &eax, &ebx, &ecx, &edx);
        if ((ecx & bit_XSAVE) == 0)
            PP_ABORT("XSAVE not supported");
        if ((ecx & bit_OSXSAVE) == 0)
            PP_ABORT("XSAVE not enabled by OS");
        if (cpuid_max < 13)
            PP_ABORT("Cannot query XSAVE area properties");

        /* Query for supported XSAVE extensions. */
        __get_cpuid_count(13, 0, &eax, &ebx, &ecx, &edx);
        unsigned int supported_subleaves_eax = eax;
        unsigned int supported_subleaves_edx = edx;

        /* XSAVE without extensions needs 512 bytes. */
        xsave_area_size = 512;

        /* Add the 64 byte header. */
        xsave_area_size += 64;

        /* Add the size of all supported XSAVE extensions. */
        for (unsigned int subleaf = 2; subleaf < 64; ++subleaf)
        {
            /* Check if the extension is supported. */
            if (subleaf < 32 && (supported_subleaves_eax & (1U << subleaf)) == 0)
                continue;
            if (subleaf >= 32 && (supported_subleaves_edx & (1U << (subleaf - 32))) == 0)
                continue;

            /* Make sure that offset + size is covered. */
            __get_cpuid_count(13, subleaf, &eax, &ebx, &ecx, &edx);
            size_t past_the_end = ebx /* offset */ + eax /* size */;
            if (past_the_end > xsave_area_size)
                xsave_area_size = past_the_end;
        }

        /* Pad to a multiple of 64 bytes, just in case. */
        if (xsave_area_size % 64 != 0)
            xsave_area_size += 64 - (xsave_area_size % 64);
    }
    return xsave_area_size;
}



/* ********************************* EVENTS ********************************* */

struct pp_thread_node;

/* pp_event_type: The type of a pp_event. */
enum pp_event_type
{
    PP_EVENT_NONE,
    PP_EVENT_EXIT,
    PP_EVENT_UNLOCK,
    PP_EVENT_CLOCK
};

/* pp_event: An event descriptor. */
struct pp_event
{
    /* Event type */
    enum pp_event_type type;

    /* Event parameters (which thread, which mutex or semaphore, ...) */
    size_t handle;
    void *argument;
};



/* ******************************** THREADS ******************************** */

/* pp_thread_node: An entry in a circular linked list of thread states. */
struct pp_thread_node
{
    /* These memory areas must be aligned properly (xsave_area: 64 bytes) */
    uintptr_t *stack;
    uintptr_t *xsave_area;

    /* For normal registers (the rest is stored in the xsave_area). We do not save segment registers. */
    struct user_regs_struct registers;

    /* Event mask we are waiting for, if any */
    struct pp_event waiting_for;

    /* Thread-local data and destructor */
    void *tls;
    void (*destructor)(void *);

    /* Index in the thread array */
    size_t array_index;

    /* The list */
    struct pp_thread_node *next;
    struct pp_thread_node *prev;
};

/* pp_thread_list: The list of userland threads in this kernel thread. */
static thread_local struct pp_thread_node *pp_thread_list = NULL;
static thread_local struct pp_thread_node *pp_cleanup_list = NULL;
static thread_local struct pp_thread_node *pp_main_thread = NULL;
static thread_local struct pp_thread_node *pp_current = NULL;

/* pp_thread_array: Holds all the thread nodes from pp_create. */
static thread_local struct pp_thread_node **pp_thread_array = NULL;
static thread_local size_t pp_thread_array_capacity = 0;
static thread_local size_t pp_thread_array_size = 0;
static thread_local size_t pp_thread_array_next = 0;
static thread_local size_t pp_skipped_dead_threads = 0;

/* pp_array_index: Access the thread array. */
static size_t pp_array_index(size_t thread_index)
{
    if (thread_index == 0)
        return thread_index;

    /* Apply pp_skipped_dead_threads to all other threads. */
    if (thread_index <= pp_skipped_dead_threads)
        return PP_INVALID_HANDLE;

    return thread_index - pp_skipped_dead_threads;
}

/* pp_alive: Checks if a thread is still alive. */
static int pp_alive(size_t thread_index)
{
    size_t index = pp_array_index(thread_index);
    if (index == PP_INVALID_HANDLE || index >= pp_thread_array_size)
        return 0;
    return pp_thread_array[index] != NULL;
}

/* pp_cleanup_thread: Cleans up a terminated thread. */
static void pp_cleanup_thread(struct pp_thread_node *node)
{
    if (node->stack)
    {
        free(node->stack);
        node->stack = NULL;
    }
    if (node->xsave_area)
    {
        free(node->xsave_area);
        node->xsave_area = NULL;
    }

    /* Call the TLS destructor, if any. */
    if (node->destructor != NULL)
        node->destructor(node->tls);

    /* Free the thread itself. */
    free(node);
}

/* pp_cleanup_main: Clean up the main thread. */
static void pp_cleanup_main()
{
    if (pp_main_thread != NULL)
    {
        pp_thread_array[pp_array_index(pp_main_thread->array_index)] = NULL;
        pp_cleanup_thread(pp_main_thread);
        pp_main_thread = NULL;
    }
}


/* **************************** PTHREADS SUPPORT **************************** */

#ifndef HALIBUT_NO_PTHREADS
/* pp_pthread_node: List entry containing a pthread handle. */
struct pp_pthread_node
{
    pthread_t handle;
    struct pp_pthread_node *prev;
    struct pp_pthread_node *next;
};

/* pp_pthreads: List of all pthreads with userland threads. */
static struct pp_pthread_node *pp_pthreads;
static pthread_mutex_t pp_pthreads_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_key_t pp_deregister_key;
static pthread_once_t pp_deregister_key_setup = PTHREAD_ONCE_INIT;

/* pp_pthread_registered: Whether this thread is already registered. */
static thread_local int pp_pthread_registered = 0;

/* pp_deregister_pthread: Deregisters a pthread from the list. */
static void pp_deregister_pthread(void *arg)
{
    pp_disable_preemption(); /* ... and it's not coming back for this thread. */
    pp_pthread_registered = 0;

    struct pp_pthread_node *node = arg;

    pthread_mutex_lock(&pp_pthreads_mutex);
    if (node->prev != node || node->next != node)
    {
        node->prev->next = node->next;
        node->next->prev = node->prev;
        if (pp_pthreads == node)
            pp_pthreads = node->next;
    }
    else
    {
        pp_pthreads = NULL;
    }
    pthread_mutex_unlock(&pp_pthreads_mutex);
    free(node);

    pp_cleanup_main();
}

/* pp_setup_deregister_key: Sets up the deregistration key. */
static void pp_setup_deregister_key()
{
    pthread_key_create(&pp_deregister_key, pp_deregister_pthread);
}

/* pp_register_pthread: Registers a pthread in the list. */
static int pp_register_pthread()
{
    struct pp_pthread_node *node = malloc(sizeof(struct pp_pthread_node));
    node->handle = pthread_self();

    int was_first;
    pthread_mutex_lock(&pp_pthreads_mutex);
    if (pp_pthreads == NULL)
    {
        node->prev = node->next = node;
        was_first = 1;
    }
    else
    {
        pp_pthreads->prev->next = node;
        node->prev = pp_pthreads->prev;
        node->next = pp_pthreads;
        pp_pthreads->prev = node;
        was_first = 0;
    }
    pp_pthreads = node;
    pthread_mutex_unlock(&pp_pthreads_mutex);

    /* Register exit handler */
    pthread_once(&pp_deregister_key_setup, pp_setup_deregister_key);
    pthread_setspecific(pp_deregister_key, node);

    return was_first;
}
#endif


/* ****************************** THREAD STATE ****************************** */

/* pp_deadlock_callback: Callback on deadlock. */
static thread_local void (*pp_deadlock_callback)(void) = NULL;

/* PP_ASM_FUNCTION: Disable as much compiler stuff as possible. */
#define PP_ASM_FUNCTION \
    __attribute__(( \
        naked, \
        no_icf, \
        no_instrument_function, \
        no_sanitize_address, \
        no_sanitize_thread, \
        no_sanitize_undefined, \
        no_split_stack, \
        no_stack_limit, \
        noinline, \
        noipa, \
        optimize("no-stack-protector"), \
    ))

/* pp_save: Save thread state */
PP_ASM_FUNCTION static void pp_save(struct pp_thread_node *into)
{
    /* Save callee-saved registers (rbx, rbp, rsp, and r12-r15) and the flags */
    __asm__ volatile (
        "pushfq;"
        "movq %%r15,    (%[registers]);"
        "movq %%r14,    8(%[registers]);"
        "movq %%r13,    16(%[registers]);"
        "movq %%r12,    24(%[registers]);"
        "movq %%rbp,    32(%[registers]);"
        "movq %%rbx,    40(%[registers]);"
        "movq %%r11,    48(%[registers]);"
        "movq %%r10,    56(%[registers]);"
        "movq %%r9,     64(%[registers]);"
        "movq %%r8,     72(%[registers]);"
        "movq %%rax,    80(%[registers]);"
        "movq %%rcx,    88(%[registers]);"
        "movq %%rdx,    96(%[registers]);"
        /* Cannot save rsi (points to registers) "movq %%rsi, 104(%[registers]);" */
        "movq %%rdi,    112(%[registers]);"
        "popq           144(%[registers]);"
        "movq %%rsp,    152(%[registers]);"
        :: [registers] "S" (&into->registers)
        : "memory"
    );
    __asm__ volatile (
        "movl $0xffffffff, %%eax;"
        "movl $0xffffffff, %%edx;"
        "xsave (%[context]);"
        "ret;"
        :: [context] "S" (into->xsave_area)
        : "memory", "rax", "rdx"
    );
}

/* pp_restore: Restore thread state */
PP_ASM_FUNCTION static void pp_restore(struct pp_thread_node *from)
{
    /* Restore xsave area, callee-saved registers, and flags, then clobber the rest */
    __asm__ volatile (
        "movl $0xffffffff, %%eax;"
        "movl $0xffffffff, %%edx;"
        "xrstor (%[context]);"
        :: [context] "S" (from->xsave_area)
        : "memory", "cc", "rax", "rdx"
    );
    __asm__ volatile (
        "movq  152(%[registers]), %%rsp;"
        "pushq 144(%[registers]);"
        "movq  112(%[registers]), %%rdi;"
        /* Cannot restore rsi (points to registers) "movq 104(%[registers]), %%rsi;" */
        "movq  96(%[registers]),  %%rdx;"
        "movq  88(%[registers]),  %%rcx;"
        "movq  80(%[registers]),  %%rax;"
        "movq  72(%[registers]),  %%r8;"
        "movq  64(%[registers]),  %%r9;"
        "movq  56(%[registers]),  %%r10;"
        "movq  48(%[registers]),  %%r11;"
        "movq  40(%[registers]),  %%rbx;"
        "movq  32(%[registers]),  %%rbp;"
        "movq  24(%[registers]),  %%r12;"
        "movq  16(%[registers]),  %%r13;"
        "movq  8(%[registers]),   %%r14;"
        "movq  (%[registers]),    %%r15;"
        "popfq;"
        "mfence;"
        "ret;"
        :: [registers] "S" (&from->registers)
        : "memory", "cc" /* And lots of others, but we don't want GCC to know that */
    );
}

/* pp_yield_to: Switch to a specific thread. */
static void pp_yield_to(struct pp_thread_node *to)
{
    pp_disable_preemption();

    /* Check if the thread can actually run, otherwise pick any other thread. */
    struct pp_thread_node *begin = to;
    unsigned waiting_on_external = 0;
    while (to->waiting_for.type != PP_EVENT_NONE)
    {
        /* If this is a PP_EVENT_CLOCK, check if the sleep has expired. */
        if (to->waiting_for.type == PP_EVENT_CLOCK)
        {
            struct timespec current;
            clock_gettime(CLOCK_MONOTONIC, &current);

            struct timespec *target = to->waiting_for.argument;
            if (current.tv_sec > target->tv_sec || (current.tv_sec == target->tv_sec && current.tv_nsec > target->tv_nsec))
            {
                /* Timer expired. */
                to->waiting_for.type = PP_EVENT_NONE;
                to->waiting_for.handle = PP_INVALID_HANDLE;
                to->waiting_for.argument = NULL;
                break;
            }

            /* This is an event that can be satisfied without any threads running. */
            ++waiting_on_external;
        }

        /* Otherwise, go to the next thread. */
        to = to->next;
        if (to == begin)
        {
            /* Our target is waiting for something, and so is everyone else. */
            if (waiting_on_external > 0)
            {
                /* Someone is waiting on a timer, no deadlock. */
                struct timespec waiting_loop = { .tv_sec = 1, .tv_nsec = 0 };
                nanosleep(&waiting_loop, NULL); /* Interrupted on SIGALRM. */
            }
            else
            {
                /* Everyone is waiting on another thread, deadlocked. */
                if (pp_deadlock_callback != NULL)
                    pp_deadlock_callback();

                /* Print verbose thread diagnostics if enabled. */
                const char *diags = getenv(PP_DIAGNOSTICS_ENV);
                if (diags && atoi(diags) > 0)
                {
                    fprintf(stderr, "\n"
                                    "======================== Deadlock detected ========================\n"
                                    "You are seeing this message because you enabled halibut's internal\n"
                                    "diagnostics via the %s environment variable.\n\n", PP_DIAGNOSTICS_ENV);

                    /* Get width of "Thread" column. */
                    int width = 6;
                    for (size_t largest_id = pp_thread_array_next; largest_id >= 1000000; largest_id /= 10)
                        ++width;
                    fprintf(stderr, "Thread%*s | Status\n", width - 6, "");
                    fprintf(stderr, "------%*s-+-------\n", width - 6, ""); /* This may look awkward later, but you need to get to one million threads first. */

                    /* Iterate through threads. */
                    do
                    {
                        char *message = NULL;
                        switch (to->waiting_for.type)
                        {
                            case PP_EVENT_NONE:
                                /* Should never result in a detected deadlock. */
                                if (asprintf(&message, "Not waiting for anything.") < 0)
                                    message = "<Could not allocate message>";
                                break;
                            case PP_EVENT_EXIT:
                                if (asprintf(&message, "Joining thread %zd.", to->waiting_for.handle) < 0)
                                    message = "<Could not allocate message>";
                                break;
                            case PP_EVENT_CLOCK:
                                /* Should never result in a detected deadlock. */
                                if (asprintf(&message, "Sleeping.") < 0)
                                    message = "<Could not allocate message>";
                                break;
                            case PP_EVENT_UNLOCK:
                                if (asprintf(&message, "Waiting for synchronization object %zd.", to->waiting_for.handle) < 0)
                                    message = "<Could not allocate message>";
                                break;
                            default:
                                if (asprintf(&message, "Waiting for unknown event.") < 0)
                                    message = "<Could not allocate message>";
                                break;
                        }
                        fprintf(stderr, "%*zd | %s\n", width, to->array_index, message);
                        free(message);

                        to = to->next;
                    } while (to != begin);

                    fprintf(stderr, "\n");
                }

                /* Actually abort. */
                PP_ABORT("Deadlock detected: No available threads.");
            }
        }
    }

    /* Update pp_current, then switch if necessary. */
    struct pp_thread_node *from = pp_current;
    pp_current = to;
    if (from != to)
    {
        pp_save(from);
        pp_restore(to);
    }

    /* We end up here after the switch completes, so we can now clean up other threads. */
    while (pp_cleanup_list != NULL)
    {
        struct pp_thread_node *node = pp_cleanup_list;
        pp_cleanup_list = node->next;

        pp_cleanup_thread(node);
    }

    pp_enable_preemption();
}

/* pp_yield: Switch to the next thread. */
static void pp_yield()
{
    pp_yield_to(pp_current->next);
}



/* **************************** SYNCHRONIZATION **************************** */

/* pp_mutex: A mutex with ownership information. */
struct pp_mutex
{
    /* The locking thread */
    size_t owner;

    /* Lock count */
    size_t count;

    /* Attributes */
    ut_mutex_attr attr;

    /* Synchronization ID. */
    size_t sync_id;
};

/* pp_semaphore: A semaphore. */
struct pp_semaphore
{
    /* Value of the semaphore. */
    size_t value;

    /* Synchronization ID. */
    size_t sync_id;
};

/* pp_sync_counter: Synchronization object counter. */
static thread_local size_t pp_sync_counter = 0;

/* pp_submit_event: Unblock all threads waiting for the specified event. */
static void pp_submit_event(size_t from_handle, enum pp_event_type type, void *argument, int notify_all)
{
    (void) argument; /* We don't process additional information right now. */

    pp_disable_preemption();

    struct pp_thread_node *iter = pp_current;
    do
    {
        if (iter->waiting_for.type == type)
        {
            if (iter->waiting_for.handle != PP_INVALID_HANDLE && iter->waiting_for.handle != from_handle)
                goto next;

            /* Unblock the thread */
            iter->waiting_for.type = PP_EVENT_NONE;
            iter->waiting_for.handle = PP_INVALID_HANDLE;
            iter->waiting_for.argument = NULL;

            if (!notify_all)
                break;
        }
next:
        iter = iter->next;
    }
    while (iter != pp_current);
    pp_enable_preemption();
}

/* pp_join: Wait until a thread has exited. */
static void pp_join(size_t target_index)
{
    if (pp_alive(target_index))
    {
        pp_current->waiting_for.type = PP_EVENT_EXIT;
        pp_current->waiting_for.handle = target_index;
        pp_current->waiting_for.argument = NULL;
    }
    pp_yield();
}



/* **************************** THREAD LIFETIME **************************** */

/* pp_mark_for_cleanup: On the next pp_yield, free this thread. */
static void pp_mark_for_cleanup(struct pp_thread_node *node)
{
    /* Add to cleanup list */
    node->next = pp_cleanup_list;
    pp_cleanup_list = node;
}

/* pp_exit_current: Exit from the current thread. */
static void pp_exit_current()
{
    pp_disable_preemption();

    if (pp_current == pp_main_thread)
        PP_ABORT("Cannot pp_exit_current() from main thread.");

    /* Submit the exit event */
    pp_thread_array[pp_array_index(pp_current->array_index)] = NULL;
    pp_submit_event(pp_current->array_index, PP_EVENT_EXIT, NULL, 1);

    /* Save the next thread (overwritten by pp_mark_for_cleanup) */
    struct pp_thread_node *next = pp_current->next;
    if (next == pp_current)
        next = pp_main_thread;

    /* Remove from the list of active threads */
    if (pp_current->prev == pp_current || pp_current->next == pp_current)
    {
        pp_thread_list = NULL;
    }
    else
    {
        pp_current->prev->next = pp_current->next;
        pp_current->next->prev = pp_current->prev;
    }

    /* Mark for cleanup on next schedule */
    pp_mark_for_cleanup(pp_current);

    /* Switch to the (saved) next thread. */
    pp_yield_to(next);
}

/* pp_create: Create a new node in the thread list. */
static struct pp_thread_node *pp_create(void (*function)(void *), void *argument)
{
    pp_disable_preemption();

    /* Allocate thread and XSAVE area */
    struct pp_thread_node *new_thread = malloc(sizeof(struct pp_thread_node));
    size_t area_size = pp_xsave_area_size();
    if (posix_memalign((void **) &new_thread->xsave_area, 64, area_size) != 0)
        PP_ABORT("Could not allocate thread context");

    new_thread->waiting_for.type = PP_EVENT_NONE;
    new_thread->waiting_for.handle = PP_INVALID_HANDLE;
    new_thread->waiting_for.argument = NULL;
    new_thread->tls = NULL;
    new_thread->destructor = NULL;

    new_thread->array_index = pp_thread_array_next++;
    ++pp_thread_array_size;

    /* Set up thread array if none exists yet. */
    if (pp_thread_array == NULL)
    {
        pp_thread_array_capacity = 0x10;
        if (posix_memalign((void **) &pp_thread_array, 64, 0x10 * sizeof(struct pp_thread_node *)) != 0)
            PP_ABORT("Could not allocate thread array");
        memset(pp_thread_array, 0, pp_thread_array_capacity);
    }

    /* If the array is full, reallocate. */
    if (pp_thread_array_size >= pp_thread_array_capacity)
    {
        struct pp_thread_node **old = pp_thread_array;
        size_t old_capacity = pp_thread_array_capacity;

        /* Filter out a block of dead threads at the start, except the main thread. */
        size_t skip = 0;
        for (; skip < old_capacity - 1 && pp_thread_array[skip + 1] == NULL; ++skip)
            ++pp_skipped_dead_threads;

        /* Reallocate. */
        pp_thread_array_capacity *= 2;
        if (posix_memalign((void **) &pp_thread_array, 64, pp_thread_array_capacity * sizeof(struct pp_thread_node *)) != 0)
            PP_ABORT("Could not reallocate thread array");
        memset(pp_thread_array, 0, pp_thread_array_capacity);

        /* Copy. */
        pp_thread_array[0] = old[0];
        for (size_t index = skip + 1; index < old_capacity; ++index)
            pp_thread_array[index - skip] = old[index];
        free(old);

        pp_thread_array_size -= skip;
    }

    /* If this is not the main thread (function != NULL), build a stack */
    if (function != NULL)
    {
        /* Find out how much stack memory it should get */
        struct rlimit stack_limit;
        if (getrlimit(RLIMIT_STACK, &stack_limit) != 0)
            PP_ABORT("Could not determine stack size");
        size_t stack_size = stack_limit.rlim_cur;

        if (posix_memalign((void **) &new_thread->stack, 64, stack_size) != 0)
            PP_ABORT("Could not allocate stack");

        /* Set up registers and stack */
        pp_save(new_thread);
        new_thread->registers.rsp = (unsigned long long) (new_thread->stack + stack_size / 8 - 16);
        new_thread->registers.rdi = (unsigned long long) argument;

        new_thread->stack[stack_size / 8 - 16] = (uintptr_t) function;
        new_thread->stack[stack_size / 8 - 15] = (uintptr_t) &pp_exit_current;
    }

    /* Insert into the list */
    pp_thread_array[pp_array_index(new_thread->array_index)] = new_thread;
    if (pp_thread_list == NULL)
    {
        new_thread->next = new_thread->prev = new_thread;
        pp_thread_list = new_thread;
    }
    else
    {
        new_thread->prev = pp_thread_list->prev; /* Ensure [tail] <- [new] */
        pp_thread_list->prev->next = new_thread; /* Ensure [tail] -> [new] */
        pp_thread_list->prev = new_thread;       /* Ensure [new] <- [head] */
        new_thread->next = pp_thread_list;       /* Ensure [new] -> [head] */
    }

    pp_enable_preemption();
    return new_thread;
}



/* ************************* PREEMPTIVE SCHEDULING ************************* */

/* PP_LOCAL_PREEMPT_SIGNAL: Local preemption signal, per-thread. */
#ifndef HALIBUT_NO_PTHREADS
#define PP_LOCAL_PREEMPT_SIGNAL SIGUSR1
#define PP_GLOBAL_PREEMPT_SIGNAL SIGALRM
#else
#define PP_LOCAL_PREEMPT_SIGNAL SIGALRM /* Preempt directly on timeout */
#endif

/* pp_preemption_disabled: Marks whether preemption is disabled. */
static thread_local int pp_preemption_disabled = 0;

/* pp_disable_preemption: Disables preemption. */
static void pp_disable_preemption()
{
    ++pp_preemption_disabled;
}

/* pp_enable_preemption: Enables preemption. */
static void pp_enable_preemption()
{
    pp_preemption_disabled = 0;
}

/* pp_preempt: The preemption timer expired, schedule a new thread. */
static void pp_preempt()
{
    pp_yield();
}

/* pp_preempt_signal: Send the thread into preemption. Must use sigaltstack. */
static void pp_preempt_signal(int signo, siginfo_t *info, void *context)
{
    (void) signo;
    (void) info;

    /* Check if preemption is enabled. */
    if (!pp_preemption_disabled)
    {
        /* Get the context */
        ucontext_t *ctx = (ucontext_t *) context;

        /* Write on their actual stack */
        ctx->uc_mcontext.gregs[REG_RSP] -= 8;
        uintptr_t *rsp = (uintptr_t *) ctx->uc_mcontext.gregs[REG_RSP];
        *rsp = ctx->uc_mcontext.gregs[REG_RIP];
        ctx->uc_mcontext.gregs[REG_RIP] = (greg_t) &pp_preempt;
    }
}

#ifndef HALIBUT_NO_PTHREADS
/* pp_preempt_global: Timeout is delivered to any pthread in the process. */
static void pp_preempt_global(int signo, siginfo_t *info, void *context)
{
    /* For every thread, forward the signal. */
    pthread_mutex_lock(&pp_pthreads_mutex);

    struct pp_pthread_node *iter = pp_pthreads;
    do
    {
        if (iter->handle != pthread_self())
            pthread_kill(iter->handle, PP_LOCAL_PREEMPT_SIGNAL);
        iter = iter->next;
    }
    while (iter != pp_pthreads);

    pthread_mutex_unlock(&pp_pthreads_mutex);

    /* Preempt current thread. */
    pp_preempt_signal(signo, info, context);
}
#endif

/* pp_setup_preemption_handlers: Sets up the signal handlers. */
static void pp_setup_preemption_handlers()
{
    /* Signal handling stack */
    stack_t sig_stack = {
        .ss_sp = malloc(SIGSTKSZ), /* TODO: Signal stack memory is leaked. */
        .ss_size = SIGSTKSZ,
        .ss_flags = 0
    };
    if (sigaltstack(&sig_stack, NULL) != 0)
        PP_ABORT("Could not set alternate signal handling stack");

    /* Local preemption handler. */
    struct sigaction local_action = {
        .sa_sigaction = pp_preempt_signal,
        .sa_flags = SA_RESTART | SA_ONSTACK | SA_SIGINFO,
    };
    if (sigaction(PP_LOCAL_PREEMPT_SIGNAL, &local_action, NULL) != 0)
        PP_ABORT("Could not register preemption handler.");

    /* Global preemption handler. */
#ifdef PP_GLOBAL_PREEMPT_SIGNAL
    struct sigaction global_action = {
        .sa_sigaction = pp_preempt_global,
        .sa_flags = SA_RESTART | SA_ONSTACK | SA_SIGINFO,
    };
    sigemptyset(&global_action.sa_mask);
    sigaddset(&global_action.sa_mask, PP_GLOBAL_PREEMPT_SIGNAL);
    if (sigaction(PP_GLOBAL_PREEMPT_SIGNAL, &global_action, NULL) != 0)
        PP_ABORT("Could not register global preemption handler.");
#endif

    /* Set up the preemption timer. */
    struct itimerval timer;

    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = PP_PREEMPTION_USECS;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_usec = PP_PREEMPTION_USECS;

    if (setitimer(ITIMER_REAL, &timer, NULL) != 0)
        PP_ABORT("Could not set preemption timer.");
}



/* ******************************** SLEEPING ******************************** */

/* pp_nanosleep: Since signals mess with sleep(), give alternatives. */
static void pp_nanosleep(const struct timespec *rqtp)
{
    /* Get current time. */
    struct timespec current;
    clock_gettime(CLOCK_MONOTONIC, &current);

    /* Add sleep time. */
    current.tv_sec += rqtp->tv_sec;
    current.tv_nsec += rqtp->tv_nsec;
    while (current.tv_nsec > 1000000000L)
    {
        ++current.tv_sec;
        current.tv_nsec -= 1000000000L;
    }

    /* Mark thread as waiting. */
    pp_current->waiting_for.type = PP_EVENT_CLOCK;
    pp_current->waiting_for.handle = PP_INVALID_HANDLE;
    pp_current->waiting_for.argument = (void *) &current;

    /* Yield. */
    pp_yield();
}



/* ***************************** INITIAL SETUP ***************************** */

/* pp_init: Set everything up if it is not already initialized. */
static void pp_init()
{
    if (pp_current != NULL)
        return; /* Already initialized. */

#ifndef HALIBUT_NO_PTHREADS
    if (pp_pthread_registered)
        return;
#endif

    /* Create the main thread. */
    pp_current = pp_main_thread = pp_create(NULL, NULL);

#ifndef HALIBUT_NO_PTHREADS
    /* Set up this pthread for userspace threading. */
    if (pp_register_pthread())
    {
        /* Set exit handler for main pthread. */
        atexit(pp_cleanup_main);

        /* If we are the first pthread, set up preemption. */
        pp_setup_preemption_handlers();
    }
#else
    /* No pthreads, always set up exit handler and preemption. */
    atexit(pp_cleanup_main);
    pp_setup_preemption_handlers();
#endif
}



/* *************************** EXPORTED FUNCTIONS *************************** */

/* ut_create: Create a userspace thread. */
void ut_create(ut_thread *thread, void (*task)(void *), void *data)
{
    pp_init();
    struct pp_thread_node *internal = pp_create(task, data);
    thread->handle = internal->array_index;
    pp_yield();
}

/* ut_create_with_tls: Wrapper for ut_create + ut_set_tls. */
void ut_create_with_tls(ut_thread *thread, void (*task)(void *), void *data, void *tls, void (*destructor)(void *))
{
    pp_init();
    struct pp_thread_node *internal = pp_create(task, data);
    thread->handle = internal->array_index;
    internal->tls = tls;
    internal->destructor = destructor;
    pp_yield();
}

/* ut_join: Wait for termination of another userspace thread. */
void ut_join(const ut_thread *thread)
{
    pp_init();
    if (thread->handle == pp_current->array_index)
        PP_ABORT("Attempting to join current thread.");
    pp_join(thread->handle);
}

/* ut_self: Returns a handle to the current thread. */
ut_thread ut_self()
{
    pp_init();
    ut_thread self = { .handle = pp_current->array_index };
    return self;
}

/* ut_running: Checks if a thread is alive. */
int ut_running(const ut_thread *thread)
{
    pp_init();
    return pp_alive(thread->handle);
}

/* ut_remote_set_tls: Set a thread's TLS pointer and destructor. */
int ut_remote_set_tls(const ut_thread *thread, void *data, void (*destructor)(void *))
{
    pp_init();
    if (!pp_alive(thread->handle))
        return -1;
    struct pp_thread_node *node = pp_thread_array[pp_array_index(thread->handle)];
    node->tls = data;
    node->destructor = destructor;
    return 0;
}

/* ut_remote_get_tls: Returns a thread's TLS pointer, or NULL. */
void *ut_remote_get_tls(const ut_thread *thread)
{
    pp_init();
    if (!pp_alive(thread->handle))
        return NULL;
    struct pp_thread_node *node = pp_thread_array[pp_array_index(thread->handle)];
    return node->tls;
}

/* ut_set_tls: Sets the current thread's TLS pointer and destructor. */
void ut_set_tls(void *data, void (*destructor)(void *))
{
    pp_init();
    pp_current->tls = data;
    pp_current->destructor = destructor;
}

/* ut_get_tls: Returns the current thread's TLS pointer. */
void *ut_get_tls()
{
    pp_init();
    return pp_current->tls;
}

/* ut_nanosleep: Like nanosleep(), but deals with the inevitable signal interrupt. */
void ut_nanosleep(const struct timespec *rqtp)
{
    pp_init();
    pp_nanosleep(rqtp);
}

/* ut_sleep: Like sleep(), but deals with the inevitable signal interrupt. */
void ut_sleep(unsigned int seconds)
{
    pp_init();
    struct timespec rqtp = { .tv_sec = seconds, .tv_nsec = 0 };
    pp_nanosleep(&rqtp);
}

/* ut_usleep: Like usleep(), but deals with the inevitable signal interrupt. */
void ut_usleep(useconds_t usec)
{
    pp_init();
    struct timespec rqtp = {
        .tv_sec = usec / 1000000,
        .tv_nsec = (long) (usec % 1000000) * 1000
    };
    pp_nanosleep(&rqtp);
}

/* ut_yield: Yield. */
void ut_yield()
{
    pp_init();
    pp_yield();
}

/* ut_set_deadlock_callback: Sets a deadlock handler. */
void ut_set_deadlock_callback(void (*callback)(void))
{
    pp_init();
    pp_deadlock_callback = callback;
}

/* ut_mutex_create: Create a mutex. */
void ut_mutex_create(ut_mutex *mutex, ut_mutex_attr flags)
{
    pp_init();
    struct pp_mutex *underlying = malloc(sizeof(struct pp_mutex));
    if (!underlying)
        PP_ABORT("Could not allocate mutex.");
    underlying->owner = PP_INVALID_HANDLE;
    underlying->count = 0;
    underlying->attr = flags;
    underlying->sync_id = pp_sync_counter++;
    mutex->handle = (void *) underlying;
}

/* ut_mutex_destroy: Destroy a mutex. */
void ut_mutex_destroy(ut_mutex *mutex)
{
    pp_init();
    struct pp_mutex *underlying = mutex->handle;
    if (!underlying)
        PP_ABORT("Tried to destroy uninitialized mutex.");
    if (underlying->owner != PP_INVALID_HANDLE)
        PP_ABORT("Tried to destroy locked mutex.");
    free(underlying);
    mutex->handle = NULL;
}

/* ut_mutex_lock: Lock the mutex. */
void ut_mutex_lock(ut_mutex *mutex)
{
    pp_init();
    struct pp_mutex *underlying = mutex->handle;
    if (!underlying)
        PP_ABORT("Tried to lock uninitialized mutex.");

    if (underlying->attr & UT_MUTEX_REENTRANT && underlying->owner == pp_current->array_index)
    {
        /* Already locked. */
        ++underlying->count;
        return;
    }

    /* Wait for the mutex to become available. */
    while (underlying->owner != PP_INVALID_HANDLE)
    {
        pp_current->waiting_for.type = PP_EVENT_UNLOCK;
        pp_current->waiting_for.handle = underlying->sync_id;
        pp_current->waiting_for.argument = NULL;
        pp_yield();
    }

    /* Lock. */
    underlying->owner = pp_current->array_index;
}

/* ut_mutex_try_lock: Attempts to lock the mutex. */
int ut_mutex_try_lock(ut_mutex *mutex)
{
    pp_init();
    struct pp_mutex *underlying = mutex->handle;
    if (!underlying)
        PP_ABORT("Tried to lock uninitialized mutex.");

    if (underlying->attr & UT_MUTEX_REENTRANT && underlying->owner == pp_current->array_index)
    {
        /* Already locked. */
        ++underlying->count;
        return 0;
    }

    /* Check if the mutex is available. */
    if (underlying->owner != PP_INVALID_HANDLE)
        return -1;

    /* Lock. */
    underlying->owner = pp_current->array_index;
    return 0;
}

/* ut_mutex_unlock: Unlock the mutex. */
void ut_mutex_unlock(ut_mutex *mutex)
{
    pp_init();
    struct pp_mutex *underlying = mutex->handle;
    if (!underlying)
        PP_ABORT("Tried to unlock uninitialized mutex.");
    if (underlying->owner == PP_INVALID_HANDLE)
        PP_ABORT("Tried to unlock unlocked mutex.");
    if (!(underlying->attr & UT_MUTEX_ANONYMOUS) && underlying->owner != pp_current->array_index)
        PP_ABORT("Tried to unlock mutex not held by the current thread.");

    /* Handle reentrant mutexes. */
    if (underlying->attr & UT_MUTEX_REENTRANT)
    {
        --underlying->count;
        if (underlying->count > 0)
        {
            /* Still locked, because reentrant. */
            return;
        }
    }

    /* Unlock the mutex. */
    underlying->owner = PP_INVALID_HANDLE;

    /* Send unlock event to all other threads waiting for this lock. */
    pp_submit_event(underlying->sync_id, PP_EVENT_UNLOCK, NULL, 0);
}

/* ut_mutex_owner: Get the current holder of a mutex. */
ut_thread ut_mutex_owner(ut_mutex *mutex)
{
    pp_init();
    struct pp_mutex *underlying = mutex->handle;
    if (!underlying)
        PP_ABORT("Tried to query owner of uninitialized mutex.");

    ut_thread thread = { .handle = underlying->owner };
    return thread;
}

/* ut_semaphore_create: Create a semaphore. */
void ut_semaphore_create(ut_semaphore *semaphore, size_t value)
{
    pp_init();
    struct pp_semaphore *underlying = malloc(sizeof(struct pp_semaphore));
    if (!underlying)
        PP_ABORT("Could not allocate semaphore.");
    underlying->value = value;
    underlying->sync_id = pp_sync_counter++;
    semaphore->handle = (void *) underlying;
}

/* ut_semaphore_destroy: Destroy a semaphore. */
void ut_semaphore_destroy(ut_semaphore *semaphore)
{
    pp_init();
    struct pp_semaphore *underlying = semaphore->handle;
    if (!underlying)
        PP_ABORT("Tried to destroy uninitialized semaphore.");
    free(underlying);
    semaphore->handle = NULL;
}

/* ut_semaphore_wait: Decrement the semaphore. */
void ut_semaphore_wait(ut_semaphore *semaphore)
{
    pp_init();
    struct pp_semaphore *underlying = semaphore->handle;
    if (!underlying)
        PP_ABORT("Tried to acquire uninitialized semaphore.");

    /* Wait for the semaphore to become available. */
    while (underlying->value == 0)
    {
        pp_current->waiting_for.type = PP_EVENT_UNLOCK;
        pp_current->waiting_for.handle = underlying->sync_id;
        pp_current->waiting_for.argument = NULL;
        pp_yield();
    }

    /* Acquire. */
    --underlying->value;
}

/* ut_semaphore_nonblocking_wait: Attempts to decrement the semaphore. */
int ut_semaphore_nonblocking_wait(ut_semaphore *semaphore)
{
    pp_init();
    struct pp_semaphore *underlying = semaphore->handle;
    if (!underlying)
        PP_ABORT("Tried to acquire uninitialized semaphore.");

    /* Check if the semaphore is available. */
    if (underlying->value == 0)
        return -1;

    /* Acquire. */
    --underlying->value;
    return 0;
}

/* ut_semaphore_signal: Increment the semaphore. */
void ut_semaphore_signal(ut_semaphore *semaphore)
{
    pp_init();
    struct pp_semaphore *underlying = semaphore->handle;
    if (!underlying)
        PP_ABORT("Tried to release uninitialized semaphore.");

    ++underlying->value;

    /* Send unlock event to all other threads waiting for this semaphore. */
    pp_submit_event(underlying->sync_id, PP_EVENT_UNLOCK, NULL, 0);
}

/* ut_cv_create: Create a condition variable. */
void ut_cv_create(ut_cv *cv)
{
    pp_init();
    cv->handle = pp_sync_counter++;
}

/* ut_cv_wait: Wait on a condition variable. */
void ut_cv_wait(ut_cv *cv)
{
    pp_current->waiting_for.type = PP_EVENT_UNLOCK;
    pp_current->waiting_for.handle = cv->handle;
    pp_current->waiting_for.argument = NULL;
    pp_yield();
}

/* ut_cv_notify: Notify one thread waiting on the condition variable. */
void ut_cv_notify(ut_cv *cv)
{
    pp_submit_event(cv->handle, PP_EVENT_UNLOCK, NULL, 0);
}

/* ut_cv_notify_all: Notify all threads waiting on the condition variable. */
void ut_cv_notify_all(ut_cv *cv)
{
    pp_submit_event(cv->handle, PP_EVENT_UNLOCK, NULL, 1);
}

/* ut_internal...: Internal helper functions published for curious users. Note the lack of pp_init(). */
size_t ut_internal_thread_id(ut_thread thread)
{
    return thread.handle;
}
size_t ut_internal_mutex_id(ut_mutex mutex)
{
    struct pp_mutex *underlying = mutex.handle;
    if (!underlying)
        return PP_INVALID_HANDLE;
    return underlying->sync_id;
}
size_t ut_internal_semaphore_id(ut_semaphore semaphore)
{
    struct pp_semaphore *underlying = semaphore.handle;
    if (!underlying)
        return PP_INVALID_HANDLE;
    return underlying->sync_id;
}
size_t ut_internal_cv_id(ut_cv cv)
{
    return cv.handle;
}
