#ifndef HALIBUT_H
#define HALIBUT_H

#include <stddef.h>
#include <time.h>
#include <unistd.h>



/* ******************************** THREADS ******************************** */

/*!
 * \brief A thread handle
 *
 * A \c ut_thread is a handle to a user-level thread.
 *
 * \warning The details of this structure are implementation-defined and may
 *          change without notice.
 */
typedef struct { size_t handle; } ut_thread;

/*!
 * \brief Creates a user-level thread.
 * \param thread Store a handle to the created thread at this location.
 * \param task   The function to run within this thread.
 * \param data   This pointer will be passed to the \c task function.
 *
 * The \c ut_create function creates a new user-level thread within the calling
 * process or kernel thread. It is automatically registered with the scheduler.
 */
void ut_create(ut_thread *thread, void (*task)(void *), void *data);

/*!
 * \brief Creates a user-level thread with TLS already initialized.
 * \param thread     Store a handle to the created thread at this location.
 * \param task       The function to run within this thread.
 * \param data       This pointer will be passed to the \c task function.
 * \param tls        Pointer to this thread's TLS data.
 * \param destructor Destructor for this thread.
 * \see \ref ut_create, \ref ut_set_tls
 *
 * This function avoids the race between \ref ut_set_remote_tls and
 * \ref ut_create when the thread's task function immediately makes use of the
 * thread's TLS data.
 */
void ut_create_with_tls(ut_thread *thread, void (*task)(void *), void *data, void *tls, void (*destructor)(void *));

/*!
 * \brief Waits for the specified user-level thread to finish.
 * \param thread The thread to wait for.
 *
 * \warning Users should ensure that \c thread is not the current thread.
 */
void ut_join(const ut_thread *thread);

/*!
 * \brief Returns a handle to the current thread.
 * \return A handle to the current thread.
 */
ut_thread ut_self();

/*!
 * \brief Checks whether a thread is still running.
 * \param thread The target thread.
 * \return 1 if the thread is running, otherwise 0.
 */
int ut_running(const ut_thread *thread);

/*!
 * \brief Sets the target thread's TLS data.
 * \param thread     The target thread.
 * \param data       A pointer to arbitrary data.
 * \param destructor A function to be called upon destruction of the thread, or
 *                   \c NULL to disable the destructor.
 *
 * \return If the target thread is no longer running, this function has no
 *         effect, and returns -1; in that case, the destructor will not be
 *         called. Otherwise, this function returns 0.
 */
int ut_set_remote_tls(const ut_thread *thread, void *data, void (*destructor)(void *));

/*!
 * \brief Obtains the target thread's TLS pointer.
 * \param thread     The target thread.
 *
 * \return The TLS pointer of the target thread, as set via
 *         \ref ut_remote_set_tls.
 */
void *ut_get_remote_tls(const ut_thread *thread);

/*!
 * \brief Sets the current thread's TLS data.
 * \param data       A pointer to arbitrary data.
 * \param destructor A function to be called upon destruction of the thread, or
 *                   \c NULL to disable the destructor.
 * \see \ref ut_remote_set_tls
 *
 * Unlike \ref ut_remote_set_tls, this function does not fail; the current
 * thread is always running.
 */
void ut_set_tls(void *data, void (*destructor)(void *));

/*!
 * \brief Obtains the current thread's TLS pointer.
 * \return The TLS pointer of the current thread
 * \see \ref ut_remote_get_tls
 *
 * This is a convenience function to avoid having to call \ref ut_self.
 */
void *ut_get_tls();

/*!
 * \brief Suspends the current thread for the specified amount of time.
 * \param rqtp How long the current thread should sleep.
 *
 * This function behaves like nanosleep(3), but does not result in errors.
 *
 * \warning If \c rqtp.tv_nesc is greater than or equal to 1000 million,
 *          overflow may occur; in that case, the behavior is undefined.
 */
void ut_nanosleep(const struct timespec *rqtp);

/*!
 * \brief Suspends the current thread for the specified amount of time.
 * \param seconds How many seconds to suspend the current thread for.
 *
 * This function behaves like sleep(3), but does not result in errors.
 */
void ut_sleep(unsigned int seconds);

/*!
 * \brief Suspends the current thread for the specified amount of time.
 * \param usec How many microseconds to suspend the current thread for.
 *
 * This function behaves like usleep(3), but does not result in errors.
 */
void ut_usleep(useconds_t usec);

/*!
 * \brief Yields control to the scheduler.
 * This function behaves much like \c sched_yield(3) on a kernel thread level.
 */
void ut_yield();



/* **************************** SYNCHRONIZATION **************************** */

/*!
 * \brief A handle to a mutex.
 *
 * \see \ref ut_mutex_create, \ref ut_mutex_destroy, \ref ut_mutex_lock,
 *      \ref ut_mutex_unlock, \ref ut_mutex_try_lock.
 *
 * To enable recursive locking on mutexes, use the \c UT_MUTEX_REENTRANT
 * flag. To disable ownership checking when unlocking mutexes, use the
 * \c UT_MUTEX_ANONYMOUS flag.
 *
 * \warning The details of this structure are implementation-defined and may
 *          change without notice.
 */
typedef struct { void *handle; } ut_mutex;

/*!
 * \brief Properties of a mutex.
 */
typedef enum {
    UT_MUTEX_STANDARD = 0,
    UT_MUTEX_REENTRANT = 1,
    UT_MUTEX_ANONYMOUS = 2,
} ut_mutex_attr;

/*!
 * \brief Creates a new mutex.
 * \param mutex Store a handle to the new mutex at this location.
 * \param flags Mutex flags, see \ref ut_mutex_attr
 */
void ut_mutex_create(ut_mutex *mutex, ut_mutex_attr flags);

/*!
 * \brief Destroys a mutex.
 * \param mutex A handle to the mutex.
 *
 * If the mutex is currently locked, the behavior is undefined.
 */
void ut_mutex_destroy(ut_mutex *mutex);

/*!
 * \brief Locks the given mutex.
 * \param mutex A handle to the mutex.
 */
void ut_mutex_lock(ut_mutex *mutex);

/*!
 * \brief Attempts to lock the given mutex.
 * \param mutex A handle to the mutex.
 * \return 0 if the mutex was locked successfully, -1 otherwise.
 */
int ut_mutex_try_lock(ut_mutex *mutex);

/*!
 * \brief Unlocks the given mutex.
 * \param mutex A handle to the mutex.
 *
 * If the mutex is not currently locked by the thread invoking this function,
 * the behavior is undefined.
 */
void ut_mutex_unlock(ut_mutex *mutex);

/*!
 * \brief Returns a handle to the current owner of the given mutex.
 * \param mutex A handle to the mutex.
 * \return A handle to the thread currently holding the mutex, or an invalid
 *         thread handle.
 *
 * If the mutex is not currently locked, this function returns an invalid
 * handle. You should use \ref ut_running to determine whether this is a valid
 * handle.
 */
ut_thread ut_mutex_owner(ut_mutex *mutex);

/*!
 * \brief A handle to a semaphore.
 *
 * \see \ref ut_semaphore_create, \ref ut_semaphore_destroy,
 *      \ref ut_semaphore_acquire, \ref ut_semaphore_try_acquire,
 *      \ref ut_semaphore_release.
 *
 * \warning The details of this structure are implementation-defined and may
 *          change without notice.
 */
typedef struct { void *handle; } ut_semaphore;

/*!
 * \brief Creates a new semaphore.
 * \param semaphore Store a handle to the new semaphore at this location.
 * \param value     Initial value of the semaphore.
 */
void ut_semaphore_create(ut_semaphore *semaphore, size_t value);

/*!
 * \brief Destroys the semaphore.
 * \param semaphore A handle to the semaphore.
 */
void ut_semaphore_destroy(ut_semaphore *semaphore);

/*!
 * \brief Waits until the semaphore's value is greater than or equal to one,
 *        then decrements the semaphore (P/acquire operation).
 * \param semaphore A handle to the semaphore.
 */
void ut_semaphore_wait(ut_semaphore *semaphore);

/*!
 * \brief Tries to decrement the semaphore (P/acquire operation).
 * \param semaphore A handle to the semaphore.
 * \return 0 if the semaphore could be decremented, otherwise -1.
 *
 * If the semaphore's value is already zero, it is not modified, and this
 * function returns -1. Otherwise, it behaves like \ref ut_semaphore_acquire,
 * except that non-blocking behavior is always guaranteed.
 */
int ut_semaphore_nonblocking_wait(ut_semaphore *semaphore);

/*!
 * \brief Increments the semaphore (V/release operation).
 * \param semaphore A handle to the targeted semaphore.
 */
void ut_semaphore_signal(ut_semaphore *semaphore);

/*!
 * \brief A handle to a condition variable.
 *
 * \see \ref ut_cv_create, \ref ut_cv_wait,
 *      \ref ut_cv_notify, \ref ut_cv_notify_all.
 *
 * Since condition variables do not carry any state, you do not need to destroy
 * them; accordingly, there is no \c ut_cv_destroy.
 *
 * \warning The details of this structure are implementation-defined and may
 *          change without notice.
 */
typedef struct { size_t handle; } ut_cv;

/*!
 * \brief Creates a new condition variable.
 * \param cv Store a handle to the new condition variable at this location.
 */
void ut_cv_create(ut_cv *cv);

/*!
 * \brief Waits until the condition variable is notified.
 * \param cv A handle to the condition variable.
 */
void ut_cv_wait(ut_cv *cv);

/*!
 * \brief Notify one thread waiting on the condition variable.
 * \param cv A handle to the condition variable.
 */
void ut_cv_notify(ut_cv *cv);

/*!
 * \brief Notify all threads waiting on the condition variable.
 * \param cv A handle to the targeted condition variable.
 */
void ut_cv_notify_all(ut_cv *cv);



/* ************************** DEBUGGING UTILITIES ************************** */

/*!
 * \brief Sets a callback to be invoked when a deadlock is detected.
 * \param callback The callback function.
 *
 * Note that the program will still abort after your callback is finished,
 * this function exists only to give you an opportunity to perform your own
 * deadlock debugging.
 */
void ut_set_deadlock_callback(void (*callback)(void));

/*!
 * \brief Obtains the internal ID of any object.
 * \param object A handle to any thread, mutex, semaphore, condition variable,
 *               etc. (although the compiler must know its type).
 *
 * Note that unlike most other functions, \c ut_internal_id does not take its
 * argument by pointer. This enables swift chaining of expressions such as
 * \c ut_internal_id(ut_mutex_owner(...)).
 *
 * The returned ID is internal and only useful to correlate your objects with
 * diagnostic messages emitted when \c HALIBUT_DIAGNOSTICS=1.
 * This macro is only available if C11 is enabled (__STDC_VERSION__ >= 201112L).
 * Otherwise, you can use \c ut_internal_*_id directly.
 */
#if __STDC_VERSION__ >= 201112L
#define ut_internal_id(object) \
    _Generic((object), \
        ut_thread:      ut_internal_thread_id, \
        ut_mutex:       ut_internal_mutex_id, \
        ut_semaphore:   ut_internal_semaphore_id, \
        ut_cv:          ut_internal_cv_id \
    )(object)
#endif

size_t ut_internal_thread_id(ut_thread thread);
size_t ut_internal_mutex_id(ut_mutex mutex);
size_t ut_internal_semaphore_id(ut_semaphore semaphore);
size_t ut_internal_cv_id(ut_cv cv);

#endif
