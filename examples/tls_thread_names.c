#include <halibut.h>

#include <stdio.h>

void demo(void *ptr)
{
    int demostack = 0;
    useconds_t delay = (useconds_t)(unsigned long long) ptr;

    printf("[%s]\t Stack location from this thread: %p\n", (const char *) ut_get_tls(), (void *) &demostack);
    ut_yield();

    printf("[%s]\t Starting loop\n", (const char *) ut_get_tls());
    for (int i = 0; i < 100; ++i)
    {
        ut_usleep(delay);
        printf("[%s]\t\t Iteration %d\n", (const char *) ut_get_tls(), i);
    }
    printf("[%s]\t Loop finished\n", (const char *) ut_get_tls());
}

void dtor(void *ptr)
{
    printf("[%s]\t TLS destructor\n", (const char *) ptr);
}

int main(void)
{
    int mainstack = 0;

    ut_set_tls("\x1b[42mMain thread\x1b[0m", dtor);
    printf("[%s]\t Stack location from this thread: %p\n", (const char *) ut_get_tls(), (void *) &mainstack);
    ut_yield();

    printf("[%s]\t Setting up threads\n", (const char *) ut_get_tls());

    ut_thread t00;
    ut_create_with_tls(&t00, demo, (void *) 30000, "\x1b[41mThread 0000\x1b[0m", dtor);
    printf("[%s]\t Back from creating thread\n", (const char *) ut_get_tls());
    ut_yield();

    ut_thread t01;
    ut_create_with_tls(&t01, demo, (void *) 50000, "\x1b[46mThread 0001\x1b[0m", dtor);

    printf("[%s]\t Starting loop\n", (const char *) ut_get_tls());
    for (int i = 0; i < 50; ++i)
    {
        ut_usleep(75000);
        printf("[%s]\t\t Iteration %d\n", (const char *) ut_get_tls(), i);
    }
    printf("[%s]\t Loop finished\n", (const char *) ut_get_tls());

    ut_join(&t00);
    ut_join(&t01);
    printf("[%s]\t All threads joined\n", (const char *) ut_get_tls());
}
