#include <halibut.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Settings */
static int philosopher_count = 5;
static int iteration_count = 25;
static int should_deadlock = 1;

/* Threading */
static ut_thread *threads;
static ut_mutex *forks;

/* Random delays */
void seed_random() { srand(time(NULL)); }
useconds_t random_delay() { return (rand() & 0xFFFF) + 0x8000; }

/* Thread function for each of the philosophers. */
void philosopher(void *arg)
{
    int index = (int) (intptr_t) arg;
    int self = ut_internal_id(ut_self());

    int left = index % philosopher_count;
    int right = (index + 1) % philosopher_count;

    if (!should_deadlock && left > right)
    {
        /* Fix deadlocks via resource ordering if --ok is given. */
        int tmp = left;
        left = right;
        right = tmp;
    }

    for (int iteration = 0; iteration < iteration_count; ++iteration)
    {
        /* Take the left fork. */
        printf("[%02d %03d] Taking left fork (%d)\n", self, iteration, left);
        ut_mutex_lock(&forks[left]);

        /*
         * Encourage another thread to take the next fork.
         * This essentially guarantees the deadlock if should_deadlock is set.
         */
        ut_yield();

        /* Take the right fork. */
        printf("[%02d %03d] Taking right fork (%d)\n", self, iteration, right);
        ut_mutex_lock(&forks[right]);

        /* Wait a bit. */
        printf("[%02d %03d] Eating\n", self, iteration);
        ut_usleep(random_delay());

        /* Release the right fork. */
        printf("[%02d %03d] Releasing right fork (%d)\n", self, iteration, right);
        ut_mutex_unlock(&forks[right]);

        /* Release the left fork. */
        printf("[%02d %03d] Releasing left fork (%d)\n", self, iteration, left);
        ut_mutex_unlock(&forks[left]);

        ut_usleep(random_delay());
    }
}

void on_deadlock(void)
{
    printf(
        "**** Deadlock detected ****\n"
        "Lock state:\n"
    );
    for (int index = 0; index < philosopher_count; ++index)
    {
        size_t owner_id = ut_internal_id(ut_mutex_owner(&forks[index]));
        size_t sync_id = ut_internal_id(forks[index]);
        printf("    Fork %d (object %zd) is held by thread %zd\n", index, sync_id, owner_id);
    }
}

int main(int argc, char *argv[])
{
    seed_random();
    ut_set_deadlock_callback(on_deadlock);

    /* Parse arguments. */
    int c;
    while ((c = getopt(argc, argv, "p:i:k")) != -1)
    {
        switch (c)
        {
        case 'p':
            philosopher_count = atoi(optarg);
            if (philosopher_count <= 0)
            {
                fprintf(stderr, "Invalid number of philosophers.\n");
                return 1;
            }
            break;
        case 'i':
            iteration_count = atoi(optarg);
            if (iteration_count <= 0)
            {
                fprintf(stderr, "Invalid number of iterations.\n");
                return 1;
            }
            break;
        case 'k':
            should_deadlock = 0;
            break;
        case '?':
            if (optopt == 'p' || optopt == 'i')
                fprintf(stderr, "Option -%c requires an argument.\n", optopt);
            else if (optopt == 'h' || optopt == '?')
                fprintf(stderr, "Usage: %s [-p philosophers] [-i iterations] [-k]\nSpecify -k to prevent the deadlock.\n", argv[0]);
            else if (isprint(optopt))
                fprintf(stderr, "Unknown option: -%c\n", optopt);
            else
                fprintf(stderr, "Invalid option character: \\x%02x", optopt);
            return 1;
        default:
            fprintf(stderr, "Failed to parse arguments.\n");
            return 1;
        }
    }

    threads = calloc(philosopher_count, sizeof(ut_thread));
    forks = calloc(philosopher_count, sizeof(ut_mutex));

    /* Initialize mutexes. */
    for (int index = 0; index < philosopher_count; ++index)
    {
        ut_mutex_create(&forks[index], 0);
        ut_mutex_lock(&forks[index]);
    }

    /* Start philosophers. */
    for (int index = 0; index < philosopher_count; ++index)
        ut_create(&threads[index], philosopher, (void *) (intptr_t) index);

    /* Unlock all mutexes. */
    for (int index = 0; index < philosopher_count; ++index)
        ut_mutex_unlock(&forks[index]);

    /* Join all threads. */
    for (int index = 0; index < philosopher_count; ++index)
        ut_join(&threads[index]);

    /* Destroy mutexes. */
    for (int index = 0; index < philosopher_count; ++index)
        ut_mutex_destroy(&forks[index]);

    free(threads);
    free(forks);
}
