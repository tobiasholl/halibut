#include <halibut.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int buffer_size = 8;
int item_count = 200;
int producers = 4;
int consumers = 2;

void seed_random() { srand(time(NULL)); }
void random_delay() { ut_usleep((rand() & 0x3FFF) + 0x4000); }

ut_cv go;

typedef struct {
    int *buffer;
    int in;
    int out;
    ut_mutex     lock;
    ut_semaphore not_full;
    ut_semaphore not_empty;
    int completed;
} ringbuf_t;

void producer(void *arg)
{
    ringbuf_t *ring = (ringbuf_t *) arg;

    ut_cv_wait(&go);

    int bonus = ut_internal_id(ut_self()) * item_count;
    for (int item = 0; item < item_count; ++item)
    {
        int item_id = bonus + item;
        ut_semaphore_wait(&ring->not_full);
        ut_mutex_lock(&ring->lock);
        ring->buffer[ring->in] = item_id;
        ring->in = (ring->in + 1) % buffer_size;
        ut_mutex_unlock(&ring->lock);
        ut_semaphore_signal(&ring->not_empty);
        random_delay();
    }

    printf("[%zd] Producer done.\n", ut_internal_id(ut_self()));
}

void consumer(void *arg)
{
    ringbuf_t *ring = (ringbuf_t *) arg;

    ut_cv_wait(&go);

    size_t id = ut_internal_id(ut_self());
    for (;;)
    {
        ut_semaphore_wait(&ring->not_empty);
        if (ring->completed >= producers * item_count)
            break;
        ut_mutex_lock(&ring->lock);
        printf("[%02zd] Consumed %5d (%5.1f%%)\n", id, ring->buffer[ring->out], (++ring->completed) * 100.0 / (producers * item_count));
        ring->out = (ring->out + 1) % buffer_size;
        ut_mutex_unlock(&ring->lock);
        if (ring->completed >= producers * item_count)
        {
            /* Guarded appropriately, wake the other. */
            ut_semaphore_signal(&ring->not_empty);
            break;
        }
        ut_semaphore_signal(&ring->not_full);
        random_delay();
    }

    printf("[%zd] Consumer done.\n", id);
}

int main(int argc, char *argv[])
{
    seed_random();

    int c;
    while ((c = getopt(argc, argv, "b:c:i:p:")) != -1)
    {
        switch (c)
        {
        case 'b':
            buffer_size = atoi(optarg);
            if (buffer_size <= 0)
            {
                fprintf(stderr, "Invalid buffer size.\n");
                return 1;
            }
            break;
        case 'c':
            consumers = atoi(optarg);
            if (consumers <= 0)
            {
                fprintf(stderr, "Invalid number of consumers.\n");
                return 1;
            }
            break;
        case 'i':
            item_count = atoi(optarg);
            if (item_count <= 0)
            {
                fprintf(stderr, "Invalid number of items per producer.\n");
                return 1;
            }
            break;
        case 'p':
            producers = atoi(optarg);
            if (producers <= 0)
            {
                fprintf(stderr, "Invalid number of producers.\n");
                return 1;
            }
            break;
        case '?':
            if (optopt == 'b' || optopt == 'c' || optopt == 'i' || optopt == 'p')
                fprintf(stderr, "Option -%c requires an argument.\n", optopt);
            else if (optopt == 'h' || optopt == '?')
                fprintf(stderr, "Usage: %s [-b buffer size] [-c consumers] [-i items per producer] [-p producers]\n", argv[0]);
            else if (isprint(optopt))
                fprintf(stderr, "Unknown option: -%c\n", optopt);
            else
                fprintf(stderr, "Invalid option character: \\x%02x", optopt);
            return 1;
        default:
            fprintf(stderr, "Failed to parse arguments.\n");
            return 1;
        }
    }

    ut_cv_create(&go);

    ringbuf_t *ring = malloc(sizeof(ringbuf_t));
    ring->buffer = calloc(buffer_size, sizeof(int));
    ring->in = 0;
    ring->out = 0;

    ut_mutex_create(&ring->lock, UT_MUTEX_STANDARD);
    ut_semaphore_create(&ring->not_full, buffer_size);
    ut_semaphore_create(&ring->not_empty, 0);

    printf("Ring ready.\n");

    ut_thread *producer_threads = calloc(producers, sizeof(ut_thread));
    ut_thread *consumer_threads = calloc(consumers, sizeof(ut_thread));

    for (int pt = 0; pt < producers; ++pt)
        ut_create(&producer_threads[pt], producer, ring);

    for (int ct = 0; ct < consumers; ++ct)
        ut_create(&consumer_threads[ct], consumer, ring);

    printf("Threads ready.\n");
    ut_sleep(1); /* For dramatic effect. */

    ut_cv_notify_all(&go);

    for (int pt = 0; pt < producers; ++pt)
        ut_join(&producer_threads[pt]);

    for (int ct = 0; ct < consumers; ++ct)
        ut_join(&consumer_threads[ct]);

    printf("Done.\n");

    free(consumer_threads);
    free(producer_threads);
    free(ring->buffer);
    free(ring);
}
